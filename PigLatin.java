import java.util.Scanner;
public class PigLatin {

	 String first;
	
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Pig Latin Converter.");
		System.out.println("");
		System.out.println("Enter sentance you want converted to Pig Latin.");
		String phrase = sc.nextLine();
		String phraseSmall = phrase.toLowerCase();
		char period = '.', excla = '!', ques = '?';
		char lastChar = phraseSmall.charAt(phrase.length()-1);
		int sentLength = phraseSmall.length();
		String phraseNoPunc = phraseSmall.substring(0, sentLength-1);
		sc.close();
		char end = 0;
		if(period == lastChar){
			end = '.';
		}else if (excla == lastChar){
			end = '!';
		}else if (ques == lastChar){
			end = '?';
		}char punct = end;
		
		String array1[] = phraseNoPunc.split(" ");
		int arrayLen = array1.length;
		
		// deals with the first word only and capitalizes it
		int y = 0;
		String first;
		while(y < 1){
			if(array1[y].startsWith("a")){
				first = array1[y] + "yay";
			}else if(array1[y].startsWith("i")){
				first = array1[y] + "yay";
			}else if(array1[y].startsWith("o")){
				first = array1[y] + "yay";
			}else if(array1[y].startsWith("u")){
				first = array1[y] + "yay";
			}else if(array1[y].startsWith("y")){
				first = array1[y] + "yay";
			}else {char firstChar = array1[y].charAt(0);
				String restOfWord = array1[y].substring(1);
				first = restOfWord + firstChar + "ay";	
			}y++;
		}System.out.print(first.substring(0, 1).toUpperCase() + first.substring(1));	
		// changes rest of words
		int x = 1;
		while(x < arrayLen){
			if(array1[x].startsWith("a")){
				System.out.print(array1[x] + "yay");
			}else if(array1[x].startsWith("i")){
				System.out.print(array1[x] + "yay");
			}else if(array1[x].startsWith("o")){
				System.out.print(array1[x] + "yay");
			}else if(array1[x].startsWith("u")){
				System.out.print(array1[x] + "yay");
			}else if(array1[x].startsWith("y")){
				System.out.print(array1[x] + "yay");
			}else {char firstChar = array1[x].charAt(0);
				String restOfWord = array1[x].substring(1);
				System.out.print(restOfWord + firstChar + "ay");	
			}x++;
			System.out.print(" ");
		}
		
			
		//}
		//System.out.println(punct);
		
	}

}
